from django.shortcuts import render
from chatterbot import ChatBot
from chatterbot.trainers import *

# Create your views here.
def main_portal(request):
    if request.method== 'POST':
        ques= request.POST['post']
        print(ques)
        answer= chatbotfunc(ques)
        print(answer)
        return render(request,"main_portal.html",{"flag":1, "respond":answer})

    return render(request,"main_portal.html")

def chatbotfunc(ques):
    bot = ChatBot("mybot")

    convo = [
    "Hello",
    "Hi there!",
    "my name is bot",
    "i am doing good and how are you",
    "what is your name?",
    "How are you doing?",
    "I'm doing great.",
    "That is good to hear",
    "Thank you.",
    "You're welcome."
    ]
    
    trainer = ListTrainer(bot)
    trainer.train(convo)
    while True:
        query= ques
        answer=bot.get_response(query)
        return answer

def login(request):
    if request.method=='POST':
        obj= Teachers.objects.all()[0]
        username= request.POST['username']
        password = request.POST['password']
        print(obj.username+obj.password)
        if obj.username==username and obj.password== password :
            return render(request,"teacher_portal.html")
        return render(request,"login.html",{'flag':1})
    else:
        return render(request,"login.html")

def portal(request):
     return render(request,"teacher_portal.html")